﻿using System;
using Spaceship;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Manager
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField] private Button startButton;
        [SerializeField] private Button restartButton;
        [SerializeField] private Button quitButton;
        [SerializeField] private RectTransform menuDialog;
        [SerializeField] private RectTransform showDialog;
        [SerializeField] private PlayerSpaceship playerSpaceship;
        [SerializeField] private EnemySpaceship enemySpaceship;
        [SerializeField] private ScoreManager scoreManager;
        [SerializeField] private AudioClip SoundExpload;
        [SerializeField] private TextMeshProUGUI PlayerHP;
        public event Action OnRestarted;
        private int playerSpaceshipHp = 1500;
        private int playerSpaceshipMoveSpeed = 6;
        private int enemySpaceshipHp = 50;
        private int enemySpaceshipMoveSpeed = 2;
        private int WaveSence;

        public PlayerSpaceship spawnedPlayership;
        public static GameManager Instance { get; private set; }
        private void Awake()
        {
            WaveSence = 1;
            startButton.onClick.AddListener(OnStartButtonClicked);
            quitButton.onClick.AddListener(OnQuitButton);

            if (Instance == null)
            {
                Instance = this;
            }
            DontDestroyOnLoad(this);
        }
        public void HpAdd(int Hp)
        {
            PlayerHP.text = $"Player Hp : {Hp}";
        }
        private void OnStartButtonClicked()
        {
            menuDialog.gameObject.SetActive(false);
            StartGame();
        }
        private void OnQuitButton()
        {
            Application.Quit();
        }
        private void SpawnPlayerSpaceship()
        {
            spawnedPlayership = Instantiate(playerSpaceship);
            spawnedPlayership.Init(playerSpaceshipHp, playerSpaceshipMoveSpeed);
            spawnedPlayership.OnExploded += OnPlayerSpaceshipExploded;
        }
        private void OnPlayerSpaceshipExploded()
        {
            showDialog.gameObject.SetActive(true);
            restartButton.onClick.AddListener(Restart);
            AudioSource.PlayClipAtPoint(SoundExpload, Camera.main.transform.position, 2);
        }
        private void SpawnEnemySpaceship(int spawnEnemy, int spawnEnemy2 , int spawnEnemy3)
        {
            var spawnedEnemyShip = Instantiate( enemySpaceship, new Vector3 (spawnEnemy,spawnEnemy2,0), 
                Quaternion.identity);
            spawnedEnemyShip.Init(enemySpaceshipHp,enemySpaceshipMoveSpeed);
            spawnedEnemyShip.OnExploded += OnEnemySpaceshipExploded;
        }
        private void OnEnemySpaceshipExploded()
        {
            AudioSource.PlayClipAtPoint(SoundExpload, Camera.main.transform.position, 2);
            scoreManager.SetScore(1);
            if (Check() == true && WaveSence == 1 )
            {
                Wave2();
            }
            if (Check() == true && WaveSence == 2 )
            {
                Wave3();
            }
            if (Check() == true && WaveSence == 3 )
            {
                EndGame();
            }
        }
        private void Restart()
        {
            DestroyRemainingShip();
            menuDialog.gameObject.SetActive(true);
            showDialog.gameObject.SetActive(false);
            OnRestarted?.Invoke();
        }
        private void DestroyRemainingShip()
        {
            var remainingEnemies = GameObject.FindGameObjectsWithTag("Enemy");
            foreach (var enemy in remainingEnemies)
            {
                Destroy(enemy);
            }
            
            var remainingPlayers = GameObject.FindGameObjectsWithTag("Player");
            foreach (var player in remainingPlayers)
            {
                Destroy(player);
            }
        }
        public bool Check()
        {
            var remainingEnemies = GameObject.FindGameObjectsWithTag("Enemy");
            if (remainingEnemies.Length <= 0)
            {
                return true;
            }
            return false;
        }
        private void StartGame()//ด่าน1
        {
            scoreManager.Init(this);
            SpawnPlayerSpaceship();
            
            SpawnEnemySpaceship(-4,7,3);
            SpawnEnemySpaceship(-2,7,3);
            SpawnEnemySpaceship(4, 7, 11 );
            SpawnEnemySpaceship(2, 7, 11 );
            SoundManager.Instance.PlayBGM();  
        }
        public void Wave2()//ด่าน2
        {
            WaveSence = 2;
            SpawnEnemySpaceship(7,8,3);
            SpawnEnemySpaceship(6,8,2);
            SpawnEnemySpaceship(2,8,1);
            SpawnEnemySpaceship(-7,8,3);
            SpawnEnemySpaceship(-5,8,2);
            SpawnEnemySpaceship(-1,8,1);
        }
        public void Wave3()//ด่าน3
        {
            WaveSence = 3;
            SpawnEnemySpaceship(4,8,3);
            SpawnEnemySpaceship(5,8,2);
            SpawnEnemySpaceship(9,8,1);
            SpawnEnemySpaceship(-8,8,3);
            SpawnEnemySpaceship(-3,8,2);
            SpawnEnemySpaceship(-6,8,1);
        }
        public void EndGame()
        {
            var remainingEnemies = GameObject.FindGameObjectsWithTag("Enemy");
            if (remainingEnemies.Length <= 0)
            {
                showDialog.gameObject.SetActive(true);
                restartButton.onClick.AddListener(Restart);
            }
        }
    }
}
