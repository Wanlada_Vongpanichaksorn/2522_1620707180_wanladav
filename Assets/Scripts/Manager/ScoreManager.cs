﻿using UnityEngine;
using TMPro;

namespace Manager
{
    public class ScoreManager : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI scoreText;
        [SerializeField] private TextMeshProUGUI showScore;
        private int scoreup = 0;
        private GameManager gameManager;
        private int playScore;
        public void Init(GameManager gameManager)
        {
            this.gameManager = gameManager;
            this.gameManager.OnRestarted += OnRestarted;
            HideScore(false);
            SetScore(0);
        }
        public  void SetScore(int score)
        {
            scoreup += score;
            scoreText.text = $"Score : {scoreup.ToString()}";
            showScore.text = $"Score : {scoreup.ToString()}";
            playScore = score;
        }
        private void Awake()
        {
            Debug.Assert(scoreText != null, "ScoreText cannot null");
        }
        private void OnRestarted()
        {
            gameManager.OnRestarted -= OnRestarted;
            HideScore(true);
            scoreup = 0;
            scoreText.text = $"Player Score : {playScore}";
        }
        private void HideScore(bool hide)
        {
            scoreText.gameObject.SetActive(!hide);
            ShowScoretext();
        }
        private void ShowScoretext()
        {
            showScore.gameObject.SetActive(true);
        }
    }
}


