﻿using System;
using Manager;
using UnityEngine;

namespace Spaceship
{
   public class PlayerSpaceship : BaseSpaceShip, IDamagable
   {
      public event Action OnExploded;
      public void Start()
      {
         GameManager.Instance.HpAdd(Hp);
      }
      private void Awake()
      {
         Debug.Assert(defaultBullet != null, "DefaultBullet cannot be null");
         Debug.Assert(gunPosition != null, "GunPosition cannot be null");
         audioSource = GetComponent<AudioSource>();
      }
      public void Init(int hp, float speed)
      {
         base.Init(hp, speed, defaultBullet);
      }
      public override void Fire()
      {
         var bullet = Instantiate(defaultBullet, gunPosition.position, Quaternion.identity);
         bullet.Init(Vector2.up);
         SoundManager.Instance.Play(audioSource,SoundManager.Sound.PlayerFire );
      }
      public void TakeHit(int damage)
      {
         Hp -= damage;
         GameManager.Instance.HpAdd(Hp);
         if (Hp > 0)
         {
            return;
         }
         Explode();
      }
      public void Explode()
      {
         Debug.Assert(Hp <=0,"HP is more than Zero.");
         Destroy(gameObject);
         OnExploded?.Invoke();
      }
   } 
}


